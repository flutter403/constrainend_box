import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'constrained_box_example.dart';
import 'icon_and_name.dart';
import 'icon_and_name_column.dart';
import 'login_page.dart';
import 'pricing_details.dart';

//Row
void main() {
  runApp(const RowExample());
}

class RowExample extends StatelessWidget {
  const RowExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Row'),
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 500.0),
                child: Card(
                  color: Colors.green[200],
                  child: Row(
                    children: [
                      Container(
                          color: Colors.green[500],
                          child: const FlutterLogo(size: 150.0)),
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            PricingDetails(),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                  'Description: the Flutter logo is a beautiful blue icon.',
                                  style: TextStyle(fontSize: 18.0)),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//Column
// void main() {
//   runApp(const ColumnExample());
// }
//
// class ColumnExample extends StatelessWidget {
//   const ColumnExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'Responsive and adaptive UI in Flutter',
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: const Text('Column'),
//           ),
//           body: const SafeArea(
//             child: LoginPage(),
//           ),
//         ),
//       ),
//     );
//   }
// }

// Flexible
// void main() {
//   runApp(const FlexibleExample());
// }
//
// class FlexibleExample extends StatelessWidget {
//   const FlexibleExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'Responsive and adaptive UI in Flutter',
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: const Text('Flexible'),
//           ),
//           body: const SafeArea(
//             child: IconAndNameColumn(iconData: Icons.web, iconName: 'web'),
//           ),
//         ),
//       ),
//     );
//   }
// }

// Expanded
// void main() {
//   runApp(const ExpandedExample());
// }
//
// class ExpandedExample extends StatelessWidget {
//   const ExpandedExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'Responsive and adaptive UI in Flutter',
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: const Text('Expanded'),
//           ),
//           body: Column(
//             children: const [
//               IconAndName(iconData: Icons.web, iconName: 'web'),
//               IconAndName(iconData: Icons.house, iconName: 'house'),
//               IconAndName(iconData: Icons.dashboard, iconName: 'dashboard'),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }


// Constrained box
// void main() {
//   runApp(const ApectRatioApp());
// }
//
// class ApectRatioApp extends StatelessWidget {
//   const ApectRatioApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return DevicePreview(
//       tools: const [
//         DeviceSection(),
//       ],
//       builder: (context) => MaterialApp(
//         debugShowCheckedModeBanner: false,
//         useInheritedMediaQuery: true,
//         builder: DevicePreview.appBuilder,
//         locale: DevicePreview.locale(context),
//         title: 'Responsive and adaptive UI in Flutter',
//         theme: ThemeData(
//           primarySwatch: Colors.green,
//         ),
//         home: Scaffold(
//           backgroundColor: Colors.white,
//           appBar: AppBar(
//             title: const Text('ConstrainedBox'),
//           ),
//           body: const ConstrainedBoxExample(),
//         ),
//       ),
//     );
//   }
// }